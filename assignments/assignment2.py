
# 1. write a program to check given number is odd or even
def function1():
    print("Program to check number is odd or even")
    print(f'Enter the number:')
    num = input()
    if int(num) % 2 == 0:
        print(f'Number {num} is Even number')
    else:
        print(f'Number {num} is Odd number')

function1()

# 2.write a program to get 10 movie names from user and create a list of them
def function2():
    movies_list = []
    for i in range(10):
        print(f'Enter the movie name:')
        movie = input()
        movies_list.append(movie)
    print(f"List of movies: {movies_list}")

#function2()

# 3. considerthe following list of numbers. iterate overit and print the sum of all the numbers
#[10, 20, 30, 40, 50, 60, 70, 80, 90, 100]

def function3():
    numbers = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
    addition = 0
    for num in numbers:
        addition = addition + num
    print(f"addition: {addition}")

#function3()

# 4.print the following list in reverse order without using reserve method
#[10, 20, 30, 40, 50, 60, 70, 80, 90, 100]

def function4():
    numbers  = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]

    # for index in range(-1,(-(len(numbers))) -1, -1):
    #     print(f"index {index}")
    #     print(numbers[index])

    for index in range( 1, len(numbers) + 1):
        print(f"index -{index}")
        print(numbers[-index])

#function4()

# 5. iterate over the following list and keep printing the sum of previous number and the next number
# [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
# for example
# 30 [10 + 20]
# 50 [20 + 30]

def function5():
    numbers = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
    for index in range(2, len(numbers) + 1):
        addition = numbers[index-2] + numbers[index-1]
        print(f" {addition} [{numbers[index-2]}+{numbers[index-1]}]")

#function5()
#
# 6. write a program to print following strin
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*
def function6():
    print('-*' * 10)

#function6()

# 7. given a list of number print whether the first and last is same
def function7():
    numbers = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
    if numbers[0] == numbers[len(numbers)-1]:
        print("First and last element is same")
    else:
        print("First and last element is different")

#function7()

# 8. given a list of number print whether the alternate numbers are odd
def function8():
    numbers = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
    for index in range(1,len(numbers),2):
        if numbers[index] % 2 == 0:
            print(f"number: {numbers[index]} is even")
        else:
            print(f"number: {numbers[index]} is odd")

#function8()

# 9. given a list of number print only those numbers which are divisible by 3
def function9():
    numbers = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
    for num in numbers:
        if num % 3 == 0:
            print(f"number: {num}  is divisable by 3")

#function9()

# 10. write a program to print number tables from 2 to 20
def function10():
    for num in range(2,21):
        table  = []
        for i in range(1,11):
            table.append(num * i)
        print(f"table of {num}: {table}")

#function10()

